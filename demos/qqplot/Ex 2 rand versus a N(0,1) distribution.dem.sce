mode(1)
//
// Demo of qqplot.sci
//
// The rand() function does not follow a normal N(0,1) distribution.
scf(); qqplot(rand(100,1)); ylabel('Samples from rand()');
//========= E N D === O F === D E M O =========//
